// notes stretched
// var do_note = new Audio("/audio/1-do.wav");
// var re_note = new Audio("/audio/2-re.wav");
// var mi_note = new Audio("/audio/3-mi.wav");
// var fa_note = new Audio("/audio/4-fa.wav");
// var sol_note = new Audio("/audio/5-sol.wav");
// var la_note = new Audio("/audio/6-la.wav");
// var ti_note = new Audio("/audio/7-si.wav");
// var do_active_note = new Audio("/audio/8-do-octave.wav");

// notes short
var do_note = new Audio("./audio-short/1-do.wav");
var re_note = new Audio("./audio-short/2-re.wav");
var mi_note = new Audio("./audio-short/3-mi.wav");
var fa_note = new Audio("./audio-short/4-fa.wav");
var sol_note = new Audio("./audio-short/5-sol.wav");
var la_note = new Audio("./audio-short/6-la.wav");
var ti_note = new Audio("./audio-short/7-si.wav");
var octave_do_note = new Audio("./audio-short/8-do-octave.wav");

// notes keys
var do_button = document.getElementById("do");
var re_button = document.getElementById("re");
var mi_button = document.getElementById("mi");
var fa_button = document.getElementById("fa");
var sol_button = document.getElementById("sol");
var la_button = document.getElementById("la");
var ti_button = document.getElementById("ti");
var octave_do_button = document.getElementById("do-active");

var notesArray = [
  do_note,
  re_note,
  mi_note,
  fa_note,
  sol_note,
  la_note,
  ti_note,
  octave_do_note,
];
var buttonsArray = [
  do_button,
  re_button,
  mi_button,
  fa_button,
  sol_button,
  la_button,
  ti_button,
  octave_do_button,
];

function playNoteByKeyboard(noteToPlay, buttonToClick) {
  noteToPlay.play();
  setTimeout(() => {
    buttonToClick.style.backgroundColor = "#686868"
  }, 200);
  setTimeout(() => {
    buttonToClick.style.backgroundColor = "#d1cfcf"
  }, 400);
}

function playNotesByMouse(notes) {
  buttonsArray.map((button) => {
    switch (button.id) {
      case "do":
        button.onclick = () => notes[0].play();
        break;
      case "re":
        button.onclick = () => notes[1].play();
        break;
      case "mi":
        button.onclick = () => notes[2].play();
        break;
      case "fa":
        button.onclick = () => notes[3].play();
        break;
      case "sol":
        button.onclick = () => notes[4].play();
        break;
      case "la":
        button.onclick = () => notes[5].play();
        break;
      case "ti":
        button.onclick = () => notes[6].play();
        break;
      case "do-active":
        button.onclick = () => notes[7].play();
        break;
      default:
        break;
    }
  });
}

playNotesByMouse(notesArray);

document.addEventListener("keydown", (eventKey, index) => {
  switch (eventKey.keyCode) {
    case 68:
      playNoteByKeyboard(notesArray[0], buttonsArray[0],);
      break;
    case 82:
      playNoteByKeyboard(notesArray[1], buttonsArray[1]);
      break;
    case 77:
      playNoteByKeyboard(notesArray[2], buttonsArray[2]);
      break;
    case 70:
      playNoteByKeyboard(notesArray[3], buttonsArray[3]);
      break;
    case 83:
      playNoteByKeyboard(notesArray[4], buttonsArray[4]);
      break;
    case 76:
      playNoteByKeyboard(notesArray[5], buttonsArray[5]);
      break;
    case 84:
      playNoteByKeyboard(notesArray[6], buttonsArray[6]);
      break;
    case 79:
      playNoteByKeyboard(notesArray[7], buttonsArray[7]);
      break;
    default:
      break;
  }
});
